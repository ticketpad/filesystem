import * as express from 'express';
import * as bodyParser from "body-parser";
import * as path from 'path';
import * as fs from "fs";

var size = 0;
var location = "";
const contentRootPath = ".\\public\\";
var isRenameChecking = false;
var copyName = "";
var Permission = {
    Allow: "allow",
    Deny: "deny"
};
var accessDetails: any = null;
const pattern = /(\.\.\/)/g;

export class AccessPermission {
    public read = Boolean;
    public write = Boolean;
    public writeContents = Boolean;
    public copy = Boolean;
    public download = Boolean;
    public upload = Boolean;
    public message = String;

    constructor(read, write, writeContents, copy, download, upload, message) {
        this.read = read;
        this.write = write;
        this.writeContents = writeContents;
        this.copy = copy;
        this.download = download;
        this.upload = upload;
        this.message = message
    }
}

export class Detail {
    public response: any = {};
    public getFileDetails(ctx, contentRootPath, filterPath): any {

        var isNamesAvailable = ctx.request.body.names.length > 0 ? true : false;
        if (ctx.request.body.names.length == 0 && ctx.request.body.data != 0) {
            var nameValues: string[] = [];
            ctx.request.body.data.forEach(function(item) {
                nameValues.push(item.name);
            });
            ctx.request.body.names = nameValues;
        }
        if (ctx.request.body.names.length == 1) {
            fileDetails(ctx, contentRootPath + (isNamesAvailable ? ctx.request.body.names[0] : "")).then((data: any) => {
                if (!data.isFile) {
                    getFolderSize(ctx, contentRootPath + (isNamesAvailable ? ctx.request.body.names[0] : ""), 0);
                    data.size = getSize(size);
                    size = 0;
                }
                if (filterPath == "") {
                    data.location = path.join(filterPath, ctx.request.body.names[0]).substr(0, path.join(filterPath, ctx.request.body.names[0]).length);
                } else {
                    data.location = path.join(contentRootPath, filterPath, ctx.request.body.names[0]);
                }
                this.response = { details: data };
                this.response = JSON.stringify(this.response);
            });
        } else {
            var isMultipleLocations = false;
            isMultipleLocations = checkForMultipleLocations(ctx, contentRootPath);
            ctx.request.body.names.forEach(function(item) {
                if (fs.lstatSync(contentRootPath + item).isDirectory()) {
                    getFolderSize(ctx, contentRootPath + item, size);
                } else {
                    const stats = fs.statSync(contentRootPath + item);
                    size = size + stats.size;
                }
            });
            fileDetails(ctx, contentRootPath + ctx.request.body.names[0]).then((data: any) => {

                var names: string[] = [];
                ctx.request.body.names.forEach(function(name) {
                    if (name.split("/").length > 0) {
                        names.push(name.split("/")[name.split("/").length - 1]);
                    }
                    else {
                        names.push(name);
                    }
                });
                data.name = names.join(", ");
                data.multipleFiles = true;
                data.size = getSize(size);
                size = 0;
                if (filterPath == "") {
                    data.location = path.join(contentRootPath, filterPath).substr(0, path.join(contentRootPath, filterPath).length - 1);
                } else {
                    data.location = path.join(contentRootPath, filterPath).substr(0, path.join(contentRootPath, filterPath).length - 1);
                }
                this.response = { details: data };
                this.response = JSON.stringify(this.response);
                isMultipleLocations = false;
                location = "";
            });
        }
        return this.response;
    }

    /**
 * function copyfile and folder
 */
    public CopyFiles(ctx, contentRootPath) {
        var result: any = {}

        var fileList: any[] = [];
        var replaceFileList: any[] = [];
        var permission; var pathPermission; var permissionDenied = false;
        pathPermission = getPathPermission(ctx.request.path, false, ctx.request.body.targetData.name, contentRootPath + ctx.request.body.targetPath, contentRootPath, ctx.request.body.targetData.filterPath);
        ctx.request.body.data.forEach(function(item) {
            var fromPath = contentRootPath + item.filterPath;
            permission = getPermission(fromPath, item.name, item.isFile, contentRootPath, item.filterPath);
            var fileAccessDenied = (permission != null && (!permission.read || !permission.copy));
            var pathAccessDenied = (pathPermission != null && (!pathPermission.read || !pathPermission.writeContents));
            if (fileAccessDenied || pathAccessDenied) {
                permissionDenied = true;
                var errorMsg: any = new Error();
                errorMsg.message = fileAccessDenied ? ((permission.message !== "") ? permission.message :
                    item.name + " is not accessible. You need permission to perform the copy action.") :
                    ((pathPermission.message !== "") ? pathPermission.message :
                        ctx.request.body.targetData.name + " is not accessible. You need permission to perform the writeContents action.");
                errorMsg.code = "401";
                result = { error: errorMsg };
                result = JSON.stringify(result);
                return result;
            }
        });
        if (!permissionDenied) {
            ctx.request.body.data.forEach(function(item) {
                var fromPath: any = contentRootPath + item.filterPath + item.name;
                var toPath: any = contentRootPath + ctx.request.body.targetPath + item.name;
                checkForFileUpdate(fromPath, toPath, item, contentRootPath, ctx);
                if (!isRenameChecking) {
                    toPath = contentRootPath + ctx.request.body.targetPath + copyName;
                    if (item.isFile) {
                        fs.copyFileSync(path.join(fromPath), path.join(toPath));
                    }
                    else {
                        copyFolder(fromPath, toPath)
                    }
                    var list = item;
                    list.filterPath = ctx.request.body.targetPath;
                    list.name = copyName;
                    fileList.push(list);
                } else {
                    replaceFileList.push(item.name);
                }
            });
            if (replaceFileList.length == 0) {
                copyName = "";
                result = { files: fileList };
                result = JSON.stringify(result);
                return result;
            } else {
                isRenameChecking = false;
                var errorMsg: any = new Error();
                errorMsg.message = "File Already Exists.";
                errorMsg.code = "400";
                errorMsg.fileExists = replaceFileList;
                result = { error: errorMsg, files: [] };
                result = JSON.stringify(result);
                return result;
            }
        }
        return this.response;
    }

    /**
 * function move files and folder
 */
    public MoveFiles(ctx, contentRootPath) {
        var result: any = {}
        var fileList: any[] = [];
        var replaceFileList: any[] = [];
        var permission; var pathPermission; var permissionDenied = false;
        pathPermission = getPathPermission(ctx.request.path, false, ctx.request.body.targetData.name, contentRootPath + ctx.request.body.targetPath, contentRootPath, ctx.request.body.targetData.filterPath);
        ctx.request.body.data.forEach(function(item) {
            var fromPath = contentRootPath + item.filterPath;
            permission = getPermission(fromPath, item.name, item.isFile, contentRootPath, item.filterPath);
            var fileAccessDenied = (permission != null && (!permission.read || !permission.write));
            var pathAccessDenied = (pathPermission != null && (!pathPermission.read || !pathPermission.writeContents));
            if (fileAccessDenied || pathAccessDenied) {
                permissionDenied = true;
                var errorMsg: any = new Error();
                errorMsg.message = fileAccessDenied ? ((permission.message !== "") ? permission.message :
                    item.name + " is not accessible. You need permission to perform the write action.") :
                    ((pathPermission.message !== "") ? pathPermission.message :
                        ctx.request.body.targetData.name + " is not accessible. You need permission to perform the writeContents action.");
                errorMsg.code = "401";
                result = { error: errorMsg };
                result = JSON.stringify(result);
                return result;

            }
        });
        if (!permissionDenied) {
            ctx.request.body.data.forEach(function(item) {
                var fromPath: any = contentRootPath + item.filterPath + item.name;
                var toPath = contentRootPath + ctx.request.body.targetPath + item.name;
                checkForFileUpdate(fromPath, toPath, item, contentRootPath, ctx);
                if (!isRenameChecking) {
                    toPath = contentRootPath + ctx.request.body.targetPath + copyName;
                    if (item.isFile) {
                        var source = fs.createReadStream(path.join(fromPath));
                        var desti = fs.createWriteStream(path.join(toPath));
                        source.pipe(desti);
                        source.on('end', function() {
                            fs.unlinkSync(path.join(fromPath));
                        });
                    }
                    else {
                        MoveFolder(fromPath, toPath);
                        fs.rmdirSync(fromPath);
                    }
                    var list = item;
                    list.name = copyName;
                    list.filterPath = ctx.request.body.targetPath;
                    fileList.push(list);
                } else {
                    replaceFileList.push(item.name);
                }
            });
            if (replaceFileList.length == 0) {
                copyName = "";
                result = { files: fileList };
                result = JSON.stringify(result);
                return result;

            }
            else {
                isRenameChecking = false;
                var errorMsg: any = new Error();
                errorMsg.message = "File Already Exists.";
                errorMsg.code = "400";
                errorMsg.fileExists = replaceFileList;
                result = { error: errorMsg, files: [] };
                result = JSON.stringify(result);
                return result;

            }
        }
        return this.response;
    }
    /**
     * function to create the folder
     */
    public createFolder(ctx, filepath, contentRootPath) {

        var newDirectoryPath = path.join(contentRootPath + ctx.request.body.path, ctx.request.body.name);
        var pathPermission = getPathPermission(ctx.request.path, false, ctx.request.body.data[0].name, filepath, contentRootPath, ctx.request.body.data[0].filterPath);
        if (pathPermission != null && (!pathPermission.read || !pathPermission.writeContents)) {
            var errorMsg: any = new Error();
            errorMsg.message = Permission + ctx.request.body.data[0].name + " is not accessible. You need permission to perform the writeContents action.";
            errorMsg.code = "401";
            this.response = { error: errorMsg };
            this.response = JSON.stringify(this.response);

        }
        else {
            if (fs.existsSync(newDirectoryPath)) {
                var errorMsg: any = new Error();
                errorMsg.message = "A file or folder with the name " + ctx.request.body.name + " already exists.";
                errorMsg.code = "400";
                this.response = { error: errorMsg };
                this.response = JSON.stringify(this.response);

            } else {
                fs.mkdirSync(newDirectoryPath);
                FileManagerDirectoryContent(ctx, newDirectoryPath).then(data => {
                    this.response = { files: data };
                    this.response = JSON.stringify(this.response);
                });
            }
        }
        return this.response;
    }

    /**
 * function to delete the folder
 */
    public deleteFolder(ctx, contentRootPath) {
        var result: any = {}
        var deleteFolderRecursive = function(path) {
            if (fs.existsSync(path)) {
                fs.readdirSync(path).forEach(function(file, index) {
                    var curPath = path + "/" + file;
                    if (fs.lstatSync(curPath).isDirectory()) { // recurse
                        deleteFolderRecursive(curPath);
                    } else { // delete file
                        fs.unlinkSync(curPath);
                    }
                });
                fs.rmdirSync(path);
            }
        };
        var permission; var permissionDenied = false;
        ctx.request.body.data.forEach(function(item) {
            var fromPath = contentRootPath + item.filterPath;
            permission = getPermission(fromPath, item.name, item.isFile, contentRootPath, item.filterPath);
            if (permission != null && (!permission.read || !permission.write)) {
                permissionDenied = true;
                var errorMsg: any = new Error();
                errorMsg.message = (permission.message !== "") ? permission.message : item.name + " is not accessible. You need permission to perform the write action.";
                errorMsg.code = "401";
                result = { error: errorMsg };
                result = JSON.stringify(result);
                return result;
            }
        });
        if (!permissionDenied) {
            var promiseList: any[] = [];
            for (var i = 0; i < ctx.request.body.data.length; i++) {
                var newDirectoryPath = path.join(contentRootPath + ctx.request.body.data[i].filterPath, ctx.request.body.data[i].name);
                if (fs.lstatSync(newDirectoryPath).isFile()) {
                    promiseList.push(FileManagerDirectoryContent(ctx, newDirectoryPath, ctx.request.body.data[i].filterPath));
                } else {
                    promiseList.push(FileManagerDirectoryContent(ctx, newDirectoryPath + "/", ctx.request.body.data[i].filterPath));
                }
            }
            Promise.all(promiseList).then(data => {
                data.forEach(function(files: any) {
                    if (fs.lstatSync(path.join(contentRootPath + files.filterPath, files.name)).isFile()) {
                        fs.unlinkSync(path.join(contentRootPath + files.filterPath, files.name));
                    } else {
                        deleteFolderRecursive(path.join(contentRootPath + files.filterPath, files.name));
                    }
                });
                result = { files: data };
                result = JSON.stringify(result);
                return result;
                this.response = result;

            });
        }
        return this.response;
    }
    /**
 * function to rename the folder
 */
    public renameFolder(ctx, contentRootPath) {
        var filename = ctx.request.path.replace(/^.*[\\\/]/, '')
        var oldName = ctx.request.body.data[0].name.split("/")[ctx.request.body.data[0].name.split("/").length - 1];
        var newName = ctx.request.body.newName.split("/")[ctx.request.body.newName.split("/").length - 1];
        var permission = getPermission((contentRootPath + ctx.request.body.data[0].filterPath), oldName, ctx.request.body.data[0].isFile, contentRootPath, ctx.request.body.data[0].filterPath);
        if (permission != null && (!permission.read || !permission.write)) {
            var errorMsg: any = new Error();
            errorMsg.message = contentRootPath + ctx.request.body.data[0].filterPath + oldName + " is not accessible.  is not accessible. You need permission to perform the write action.";
            errorMsg.code = "401";
            this.response = { error: errorMsg };
            this.response = JSON.stringify(this.response);

        } else {
            var oldDirectoryPath = path.join(contentRootPath + ctx.request.body.data[0].filterPath, oldName);
            var newDirectoryPath = path.join(contentRootPath + ctx.request.body.data[0].filterPath, newName);
            if (checkForDuplicates(contentRootPath + ctx.request.body.data[0].filterPath, newName, ctx.request.body.data[0].isFile)) {
                var errorMsg: any = new Error();
                errorMsg.message = "A file or folder with the name " + ctx.request.body.name + " already exists.";
                errorMsg.code = "400";
                this.response = { error: errorMsg };

                this.response = JSON.stringify(this.response);

            } else {
                fs.renameSync(oldDirectoryPath, newDirectoryPath);
                (async () => {
                    await FileManagerDirectoryContent(ctx, newDirectoryPath + "/").then(data => {
                        this.response = { files: data };
                        this.response = JSON.stringify(this.response);
                    });
                })();
            }
        }
        return this.response;
    }
}

/**
 * function to get the file details like path, name and size
 */
function fileDetails(ctx, filepath) {
    return new Promise((resolve, reject) => {
        var cwd: any = {};
        fs.stat(filepath, function(err, stats) {
            cwd.name = path.basename(filepath);
            cwd.size = getSize(stats.size);
            cwd.isFile = stats.isFile();
            cwd.modified = stats.ctime;
            cwd.created = stats.mtime;
            cwd.type = path.extname(filepath);
            cwd.location = ctx.request.body.data[0].filterPath
            resolve(cwd);
        });
    });
}

/**
 * function to get the folder size
 */
function getFolderSize(ctx, directory, sizeValue) {
    size = sizeValue;
    var filenames = fs.readdirSync(directory);
    for (var i = 0; i < filenames.length; i++) {
        if (fs.lstatSync(directory + "/" + filenames[i]).isDirectory()) {
            getFolderSize(ctx, directory + "/" + filenames[i], size);
        } else {
            const stats = fs.statSync(directory + "/" + filenames[i]);
            size = size + stats.size;
        }
    }
}
function checkForMultipleLocations(ctx, contentRootPath) {
    var previousLocation = "";
    var isMultipleLocation = false;
    ctx.request.body.data.forEach(function(item) {
        if (previousLocation == "") {
            previousLocation = item.filterPath;
            location = item.filterPath;
        } else if (previousLocation == item.filterPath && !isMultipleLocation) {
            isMultipleLocation = false;
            location = item.filterPath;
        } else {
            isMultipleLocation = true;
            location = "Various Location";
        }
    });
    if (!isMultipleLocation) {
        location = contentRootPath.split("/")[contentRootPath.split("/").length - 1] + location.substr(0, location.length - 2);
    }
    return isMultipleLocation;
}

function getSize(size): void {
    var hz;
    if (size < 1024) hz = size + ' B';
    else if (size < 1024 * 1024) hz = (size / 1024).toFixed(2) + ' KB';
    else if (size < 1024 * 1024 * 1024) hz = (size / 1024 / 1024).toFixed(2) + ' MB';
    else hz = (size / 1024 / 1024 / 1024).toFixed(2) + ' GB';
    return hz;
}

function getPathPermission(path, isFile, name, filepath, contentRootPath, filterPath) {
    return getPermission(filepath, name, isFile, contentRootPath, filterPath);
}

function hasPermission(rule) {
    return ((rule == undefined) || (rule == null) || (rule == Permission.Allow)) ? true : false;
}

function getMessage(rule) {
    return ((rule.message == undefined) || (rule.message == null)) ? "" : rule.message;
}

function updateRules(filePermission, accessRule) {
    filePermission.download = hasPermission(accessRule.read) && hasPermission(accessRule.download);
    filePermission.write = hasPermission(accessRule.read) && hasPermission(accessRule.write);
    filePermission.writeContents = hasPermission(accessRule.read) && hasPermission(accessRule.writeContents);
    filePermission.copy = hasPermission(accessRule.read) && hasPermission(accessRule.copy);
    filePermission.read = hasPermission(accessRule.read);
    filePermission.upload = hasPermission(accessRule.read) && hasPermission(accessRule.upload);
    filePermission.message = getMessage(accessRule);
    return filePermission;
}
function getPermission(filepath, name, isFile, contentRootPath, filterPath) {
    var filePermission: any = new AccessPermission(true, true, true, true, true, true, "");
    if (accessDetails == null) {
        return null;
    } else {
        accessDetails.rules.forEach(function(accessRule) {
            if (isFile && accessRule.isFile) {
                var nameExtension = name.substr(name.lastIndexOf("."), name.length - 1).toLowerCase();
                var fileName = name.substr(0, name.lastIndexOf("."));
                var currentPath = contentRootPath + filterPath;
                if (accessRule.isFile && isFile && accessRule.path != "" && accessRule.path != null && (accessRule.role == null || accessRule.role == accessDetails.role)) {
                    if (accessRule.path.indexOf("*.*") > -1) {
                        var parentPath = accessRule.path.substr(0, accessRule.path.indexOf("*.*"));
                        if (currentPath.indexOf(contentRootPath + parentPath) == 0 || parentPath == "") {
                            filePermission = updateRules(filePermission, accessRule);
                        }
                    }
                    else if (accessRule.path.indexOf("*.") > -1) {
                        var pathExtension = accessRule.path.substr(accessRule.path.lastIndexOf("."), accessRule.path.length - 1).toLowerCase();
                        var parentPath = accessRule.path.substr(0, accessRule.path.indexOf("*."));
                        if (((contentRootPath + parentPath) == currentPath || parentPath == "") && nameExtension == pathExtension) {
                            filePermission = updateRules(filePermission, accessRule);
                        }
                    }
                    else if (accessRule.path.indexOf(".*") > -1) {
                        var pathName = accessRule.path.substr(0, accessRule.path.lastIndexOf(".")).substr(accessRule.path.lastIndexOf("/") + 1, accessRule.path.length - 1);
                        var parentPath = accessRule.path.substr(0, accessRule.path.indexOf(pathName + ".*"));
                        if (((contentRootPath + parentPath) == currentPath || parentPath == "") && fileName == pathName) {
                            filePermission = updateRules(filePermission, accessRule);
                        }
                    }
                    else if (contentRootPath + accessRule.path == filepath) {
                        filePermission = updateRules(filePermission, accessRule);
                    }
                }
            } else {
                if (!accessRule.isFile && !isFile && accessRule.path != null && (accessRule.role == null || accessRule.role == accessDetails.role)) {
                    var parentFolderpath = contentRootPath + filterPath;
                    if (accessRule.path.indexOf("*") > -1) {
                        var parentPath = accessRule.path.substr(0, accessRule.path.indexOf("*"));
                        if (((parentFolderpath + (parentFolderpath[parentFolderpath.length - 1] == "/" ? "" : "/") + name).lastIndexOf(contentRootPath + parentPath) == 0) || parentPath == "") {
                            filePermission = updateRules(filePermission, accessRule);
                        }
                    } else if (path.join(contentRootPath, accessRule.path) == path.join(parentFolderpath, name) || path.join(contentRootPath, accessRule.path) == path.join(parentFolderpath, name + "/")) {
                        filePermission = updateRules(filePermission, accessRule);
                    }
                    else if (path.join(parentFolderpath, name).lastIndexOf(path.join(contentRootPath, accessRule.path)) == 0) {
                        filePermission.write = hasPermission(accessRule.writeContents);
                        filePermission.writeContents = hasPermission(accessRule.writeContents);
                        filePermission.message = getMessage(accessRule);
                    }
                }
            }
        });
        return filePermission;
    }
}
/**
 *
 * function to check for exising folder or file
 */
function checkForDuplicates(directory, name, isFile) {
    var filenames = fs.readdirSync(directory);
    if (filenames.indexOf(name) == -1) {
        return false;
    } else {
        for (var i = 0; i < filenames.length; i++) {
            if (filenames[i] === name) {
                if (!isFile && fs.lstatSync(directory + "/" + filenames[i]).isDirectory()) {
                    return true;
                } else if (isFile && !fs.lstatSync(directory + "/" + filenames[i]).isDirectory()) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}
function updateCopyName(path, name, count, isFile) {
    var subName = "", extension = "";
    if (isFile) {
        extension = name.substr(name.lastIndexOf('.'), name.length - 1);
        subName = name.substr(0, name.lastIndexOf('.'));
    }
    copyName = !isFile ? name + "(" + count + ")" : (subName + "(" + count + ")" + extension);
    if (checkForDuplicates(path, copyName, isFile)) {
        count = count + 1;
        updateCopyName(path, name, count, isFile);
    }
}

function checkForFileUpdate(fromPath, toPath, item, contentRootPath, ctx) {
    var count = 1;
    var name = copyName = item.name;
    if (fromPath == toPath) {
        if (checkForDuplicates(contentRootPath + ctx.request.body.targetPath, name, item.isFile)) {
            updateCopyName(contentRootPath + ctx.request.body.targetPath, name, count, item.isFile);
        }
    } else {
        if (ctx.request.body.renameFiles.length > 0 && ctx.request.body.renameFiles.indexOf(item.name) >= 0) {
            updateCopyName(contentRootPath + ctx.request.body.targetPath, name, count, item.isFile);
        } else {
            if (checkForDuplicates(contentRootPath + ctx.request.body.targetPath, name, item.isFile)) {
                isRenameChecking = true;
            }
        }
    }
}
function copyFolder(source, dest) {
    if (!fs.existsSync(dest)) {
        fs.mkdirSync(dest);
    }
    var files = fs.readdirSync(source);
    files.forEach(function(file) {
        var curSource = path.join(source, file);
        if (fs.lstatSync(curSource).isDirectory()) {
            copyFolder(curSource, path.join(dest, file)); source
        } else {
            fs.copyFileSync(path.join(source, file), path.join(dest, file));
        }
    });
}
function MoveFolder(source, dest) {
    if (!fs.existsSync(dest)) {
        fs.mkdirSync(dest);
    }
    var files = fs.readdirSync(source);
    files.forEach(function(file) {
        var curSource = path.join(source, file);
        if (fs.lstatSync(curSource).isDirectory()) {
            MoveFolder(curSource, path.join(dest, file));
            fs.rmdirSync(curSource);
        } else {
            fs.copyFileSync(path.join(source, file), path.join(dest, file));
            fs.unlinkSync(path.join(source, file));
        }
    });
}
/**
 * returns the current working directories
 */
function FileManagerDirectoryContent(ctx, filepath, searchFilterPath?) {
    return new Promise((resolve, reject) => {
        var cwd: any = {};
        replaceRequestParams(ctx);
        fs.stat(filepath, function(err, stats) {
            cwd.name = path.basename(filepath);

            cwd.size = getSize(stats.size);
            cwd.isFile = stats.isFile();
            cwd.dateModified = stats.ctime;
            cwd.dateCreated = stats.mtime;
            cwd.type = path.extname(filepath);
            if (searchFilterPath) {
                cwd.filterPath = searchFilterPath;
            } else {
                cwd.filterPath = ctx.request.body.data.length > 0 ? getRelativePath(contentRootPath, contentRootPath + ctx.request.body.path.substring(0, ctx.request.body.path.indexOf(ctx.request.body.data[0].name))) : "";
            }
            cwd.permission = getPathPermission(ctx.request.path, cwd.isFile, (ctx.request.body.path == "/") ? "" : cwd.name, filepath, contentRootPath, cwd.filterPath);
            if (fs.lstatSync(filepath).isFile()) {
                cwd.hasChild = false;
                resolve(cwd);
            }
        });
        if (fs.lstatSync(filepath).isDirectory()) {
            fs.readdir(filepath, function(err, stats) {
                stats.forEach(stat => {
                    if (fs.lstatSync(filepath + stat).isDirectory()) {
                        cwd.hasChild = true
                    } else {
                        cwd.hasChild = false;
                    }
                    if (cwd.hasChild) return;
                });
                resolve(cwd);
            });
        }
    });
}

function replaceRequestParams(ctx) {
    ctx.request.body.path = (ctx.request.body.path && ctx.request.body.path.replace(pattern, ""));
}


function getRelativePath(rootDirectory, fullPath) {
    if (rootDirectory.substring(rootDirectory.length - 1) == "/") {
        if (fullPath.indexOf(rootDirectory) >= 0) {
            return fullPath.substring(rootDirectory.length - 1);
        }
    }
    else if (fullPath.indexOf(rootDirectory + "/") >= 0) {
        return "/" + fullPath.substring(rootDirectory.length + 1);
    }
    else {
        return "";
    }
}