// Async recursive directory walk
import * as fs from "fs";
import * as path from "path";
import { FileList } from "./file-list";

type IPromiseResolve = (a?: Promise<void>) => void;
type IPromiseResolveString = (a?: string[]) => void;
type IPromiseReject = (a?: NodeJS.ErrnoException | void) => void;
type IWalkDone = (a: NodeJS.ErrnoException | null, b?: string[]) => void;

enum EnvProperty {
    NODE_ENV = "NODE_ENV"
}

function isRegExp(obj): boolean {
    return obj && typeof (obj.test) === "function";
}

export class FileUtils {

    public isWin = process.platform === "win32";
    public slash = this.isWin ? "\\\\" : "/";

    public get isDevelopment(): boolean {
        return process.env[EnvProperty.NODE_ENV] === "development";
    }

    /**
     * Async recursive directory walk
     * @param  {string}   dir  Folder to start recursive search
     * @param  {Function} done When all is done, do callback function.
     * @return {String Array}        Returns an array of strings to files
     */
    public walk(dir: string, done: IWalkDone): void {
        // const self = this;
        let results: string[] = [];

        fs.readdir(dir, (err: NodeJS.ErrnoException | null, list: string[]) => {
            if (err) {
                return done(err);
            }

            let pending = list.length;
            if (!pending) {
                return done(null, results);
            }

            list.forEach(file => {
                file = path.resolve(dir, file);

                fs.stat(file, (err2: NodeJS.ErrnoException | null, stat: fs.Stats) => {

                    if (stat && stat.isDirectory()) {

                        this.walk(file, (err3: NodeJS.ErrnoException | null, res?: string[]) => {

                            results = results.concat(res || []);
                            if (!--pending) {
                                done(null, results);
                            }

                        });
                    } else {
                        results.push(file);
                        if (!--pending) {
                            done(null, results);
                        }
                    }
                });
            });
        });
    }

    /**
     * Serial recursive directory walk
     * @param  {string}   dir  Folder to start recursive search
     * @return {String Array}        Returns an array of strings to files
     */
    public walkSync(dir: string){

        let results: string[] = [];

        return new Promise((resolve: IPromiseResolveString, reject: IPromiseReject) => {
            fs.readdir(dir, (err: NodeJS.ErrnoException | null, list: string[]) => {
                if (err) {
                    return reject(err);
                }

                const self = this;
                let i = 0;

                (function next() {
                    let file = list[i++];
                    if (!file) {
                        return resolve(results);
                    }
                    file = path.resolve(dir, file);
                    fs.stat(file, (err2: NodeJS.ErrnoException | null, stat: fs.Stats) => {
                        if (stat && stat.isDirectory()) {
                            self.walk(file, (err3: NodeJS.ErrnoException | null, res?: string[]) => {
                                results = results.concat(res || []);
                                next();
                            });
                        } else {
                            results.push(file);
                            next();
                        }
                    });
                })();
            });
        });

    }

    /**
     * Reads a file
     * // May require to use fileUtils.readFile("./ref.csv").split(/\r?\n/) to split the lines
     * @param {string} path     path of file
     * @param {string} encoding encoding type (optional)
     */
    public readFile(exactPath: string, encoding?: BufferEncoding): string | null | Buffer {
        if (fs.existsSync(exactPath)) {
            return fs.readFileSync(exactPath, encoding || "utf8");
        } else {
            console.log("Not found:", exactPath);
            return null;
        }
    }

    /**
     * Used to write contents to a file
     * @param {string} path    path of file
     * @param {string} content contents to be written
     */
    public writefile(exactPath: string, content: string | Buffer): void {
        if (fs.writeFileSync) {
            fs.writeFileSync(exactPath, content);
        } else {
            fs.writeFile(exactPath, content, (err: NodeJS.ErrnoException | null) => {
                if (err) {
                    return console.log(err);
                }
            });
        }
    }

    /**
     * Deletes a file - throws an error if path doesn't exist
     * @param  {string}        dir  [description]
     * @param  {string}        file [description]
     * @return {Promise<void>}      [description]
     */
    public deleteFile(dir: string, file: string): Promise<void> {
        return new Promise((resolve: IPromiseResolve, reject: IPromiseReject) => {
            const filePath = path.join(dir, file);
            fs.lstat(filePath, (err: NodeJS.ErrnoException | null, stats: fs.Stats) => {
                if (err) {
                    return reject(err);
                }
                if (stats.isDirectory()) {
                    resolve(this.deleteDirectory(filePath));
                } else {
                    fs.unlink(filePath, (err2: NodeJS.ErrnoException | null) => {
                        if (err2) {
                            return reject(err2);
                        }
                        resolve();
                    });
                }
            });
        });
    }

    /**
     * Deletes a directory and all contents within it, throws an error if path doesn't exist
     * @param  {string}        dir [description]
     * @return {Promise<void>}     [description]
     */
    public deleteDirectory(dir: string): Promise<void> {
        return new Promise((resolve: IPromiseResolve, reject: IPromiseReject) => {
            fs.access(dir, (err: NodeJS.ErrnoException | null) => {
                if (err) {
                    return reject(err);
                }
                fs.readdir(dir, (err2: NodeJS.ErrnoException | null, files: string[]) => {
                    if (err2) {
                        return reject(err2);
                    }
                    Promise.all(files.map((file: string) => {
                        return this.deleteFile(dir, file);
                    })).then(() => {
                        fs.rmdir(dir, (err3: NodeJS.ErrnoException | null) => {
                            if (err3) {
                                return reject(err3);
                            }
                            resolve();
                        });
                    }).catch(reject);
                });
            });
        });
    }

    /**
     * Copies a file
     * @param {string} from [description]
     * @param {string} to   [description]
     */
    public copyFile(from: string, to: string): void {
        fs.mkdirSync(path.dirname(to), { recursive: true });
        fs.copyFileSync(from, to);
    }

    public async asyncCopyFile(source: string, target: string): Promise<void> {
        const rd = fs.createReadStream(source);
        const wr = fs.createWriteStream(target);
        try {
            return await new Promise((resolve, reject) => {
                rd.on("error", reject);
                wr.on("error", reject);
                wr.on("finish", resolve);
                rd.pipe(wr);
            });
        } catch (e) {
            rd.destroy();
            wr.end();
            throw e;
        }
    }

    /**
     * Copies a folder and contents within it
     * @param {string}      from [description]
     * @param {string}      to   [description]
     * @param {string[] |    Map<string,   boolean>} extFilter [description]
     */
    public copyFolderSync(from: string, to: string, extFilter?: string[] | Map<string, boolean>): void {
        let whitelist: Map<string, boolean>;
        if (extFilter) {
            if (Array.isArray(extFilter)) {
                whitelist = new Map();
                for (const c of extFilter) {
                    whitelist.set(`.${(c || "").toLowerCase()}`, true);
                }
            } else {
                whitelist = extFilter;
            }
        }

        fs.mkdirSync(to, { recursive: true });
        fs.readdirSync(from).forEach(element => {
            const ext = (this.extension(element) || "").toLowerCase();
            if (fs.lstatSync(path.join(from, element)).isFile()) {
                if (!whitelist || (whitelist && whitelist.has(ext))) {
                    fs.copyFileSync(path.join(from, element), path.join(to, element));
                }
            } else {
                this.copyFolderSync(path.join(from, element), path.join(to, element), whitelist);
            }
        });
    }

    public syncFileToBuffer(filepath: string): Buffer {
        // Maximum buffer size, with a default of 512 kilobytes.
        // TO-DO: make this adaptive based on the initial signature of the image
        const MaxBufferSize = 512 * 1024;

        // read from the file, synchronously
        const descriptor = fs.openSync(filepath, "r");
        const size = fs.fstatSync(descriptor).size;
        const bufferSize = Math.min(size, MaxBufferSize);
        const buffer = Buffer.alloc(bufferSize);
        fs.readSync(descriptor, buffer, 0, bufferSize, 0);
        fs.closeSync(descriptor);
        return buffer;
    }

    /**
     * Gets the file extension
     * @param  {string} filename path of the file
     * @return {string}          Returns string - .html, .json, .css
     */
    public extension(filename: string): string {
        return path.extname(filename);
    }

    public makePath(uploadPath: string): void {
        if (!fs.existsSync(uploadPath)) {
            fs.mkdirSync(uploadPath, { recursive: true });
        }
    }

    public ls(exactPath: string, callback: Function, advanced = false, filter?: string | RegExp | Function): void {
        const filelist = new FileList();
        let tmp: string | RegExp | Function;

        filelist.advanced = advanced;
        filelist.onComplete = callback;

        if (typeof (filter) === "string") {
            tmp = filter.toLowerCase();
            filelist.onFilter = (filename: string, is: boolean) => {
                return is ? true : filename.toLowerCase().includes(<string>tmp);
            };
        } else if (isRegExp(filter)) {
            tmp = <RegExp>filter;
            filelist.onFilter = (filename: string, is: boolean) => {
                return is ? true : (<RegExp>tmp).test(filename);
            };
        } else {
            filelist.onFilter = <Function>filter || null;
        }

        filelist.walk(exactPath);
    }

    public lsSync(exactPath: string, advanced = false, filter?: string | RegExp | Function): Promise<string[]> {
        return new Promise((resolve, reject) => {
            this.ls(exactPath, (results: string[]) => resolve(results), advanced, filter);
        });
    }

    // fs functions
    /**
     * Check if path already exist
     * @param  {string}  path [description]
     * @return {boolean}      [description]
     */
    public get existsSync(): Function {
        return fs.existsSync;
    }

    public get rmdir(): Function {
        return fs.rmdir;
    }

    public get unlink(): Function {
        return fs.unlink;
    }

    public get statSync(): Function {
        return fs.statSync;
    }

    public get readdirSync(): Function {
        return fs.readdirSync;
    }

    public get renameSync(): Function {
        return fs.renameSync;
    }

    public get readFileSync(): Function {
        return fs.readFileSync;
    }

}
