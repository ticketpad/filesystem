// Origin - https://www.npmjs.com/package/mime-db
// https://github.com/jshttp/mime-db/blob/master/db.json
import { dependency } from "@foal/core";
import * as express from 'express';
import * as bodyParser from "body-parser";
import * as path from 'path';
import * as fs from "fs";
import {
    fileDetails, AccessDetails, AccessRules, AccessPermission, getPermission,
    getSize, getPathPermission, fileManagerDirectoryContent
} from "./export.service";
// const element
const contentRootPath = ".\\public\\";
const pattern = /(\.\.\/)/g;
var Permission = {
    Allow: "allow",
    Deny: "deny"
};
var size = 0;
var location = "";
var isRenameChecking = false;
var copyName = "";
var Permission = {
    Allow: "allow",
    Deny: "deny"
};
var accessDetails: any = null;

export class System {
    public response: any = {};

    public async GetFiles(req) {
        return await new Promise((resolve, reject) => {
            fs.readdir(contentRootPath + req.request.body.path.replace(pattern, ""), (err, files) => {
                //handling error
                if (err) {
                    console.log(err);
                    reject(err);

                } else
                    resolve(files);
            });
        });
    }

    public getRules() {
        var details = new AccessDetails();
        var accessRuleFile = "accessRules.json";
        if (!fs.existsSync(accessRuleFile)) { return null; }
        var rawData: any = fs.readFileSync(accessRuleFile);
        if (rawData.length === 0) { return null; }
        var parsedData = JSON.parse(rawData);
        var data = parsedData.rules;
        var accessRules: any = [];
        for (var i = 0; i < data.length; i++) {
            var rule = new AccessRules(data[i].path, data[i].role, data[i].read, data[i].write, data[i].writeContents, data[i].copy, data[i].download, data[i].upload, data[i].isFile, data[i].message);
            accessRules.push(rule);
        }
        if (accessRules.length == 1 && accessRules[0].path == undefined) {
            return null;
        } else {
            details.rules = accessRules;
            details.role = parsedData.role;
            return details;
        }
    }

    public async getFileDetails(ctx, contentRootPath, filterPath): Promise<any> {

        var isNamesAvailable = ctx.request.body.names.length > 0 ? true : false;
        if (ctx.request.body.names.length == 0 && ctx.request.body.data != 0) {
            var nameValues: string[] = [];
            await ctx.request.body.data.forEach((item) => {
                nameValues.push(item.name);
            });
            ctx.request.body.names = nameValues;
        }
        if (ctx.request.body.names.length == 1) {
            await fileDetails(ctx, contentRootPath + (isNamesAvailable ? ctx.request.body.names[0] : "")).then((data: any) => {
                if (!data.isFile) {
                    getFolderSize(ctx, contentRootPath + (isNamesAvailable ? ctx.request.body.names[0] : ""), 0);
                    data.size = getSize(size);
                    size = 0;
                }
                if (filterPath == "") {
                    data.location = path.join(filterPath, ctx.request.body.names[0]).substr(0, path.join(filterPath, ctx.request.body.names[0]).length);
                } else {
                    data.location = path.join(contentRootPath, filterPath, ctx.request.body.names[0]);
                }
                this.response = { details: data };
                this.response = JSON.stringify(this.response);
            });
        } else {
            var isMultipleLocations = false;
            isMultipleLocations = checkForMultipleLocations(ctx, contentRootPath);
            await ctx.request.body.names.forEach((item) => {
                if (fs.lstatSync(contentRootPath + item).isDirectory()) {
                    getFolderSize(ctx, contentRootPath + item, size);
                } else {
                    const stats = fs.statSync(contentRootPath + item);
                    size = size + stats.size;
                }
            });
            await fileDetails(ctx, contentRootPath + ctx.request.body.names[0]).then((data: any) => {

                var names: string[] = [];
                ctx.request.body.names.forEach((name) => {
                    if (name.split("/").length > 0) {
                        names.push(name.split("/")[name.split("/").length - 1]);
                    }
                    else {
                        names.push(name);
                    }
                });
                data.name = names.join(", ");
                data.multipleFiles = true;
                data.size = getSize(size);
                size = 0;
                if (filterPath == "") {
                    data.location = path.join(contentRootPath, filterPath).substr(0, path.join(contentRootPath, filterPath).length - 1);
                } else {
                    data.location = path.join(contentRootPath, filterPath).substr(0, path.join(contentRootPath, filterPath).length - 1);
                }
                this.response = { details: data };
                this.response = JSON.stringify(this.response);
                isMultipleLocations = false;
                location = "";
            });
        }
        return this.response;
    }

    /**
 * function copyfile and folder
 */
    public async CopyFiles(ctx, contentRootPath) {
        var result: any = {}

        var fileList: any[] = [];
        var replaceFileList: any[] = [];
        var permission; var pathPermission; var permissionDenied = false;
        pathPermission = getPathPermission(ctx.request.path, false, ctx.request.body.targetData.name, contentRootPath + ctx.request.body.targetPath, contentRootPath, ctx.request.body.targetData.filterPath);
        await ctx.request.body.data.forEach((item) => {
            var fromPath = contentRootPath + item.filterPath;
            permission = getPermission(fromPath, item.name, item.isFile, contentRootPath, item.filterPath);
            var fileAccessDenied = (permission != null && (!permission.read || !permission.copy));
            var pathAccessDenied = (pathPermission != null && (!pathPermission.read || !pathPermission.writeContents));
            if (fileAccessDenied || pathAccessDenied) {
                permissionDenied = true;
                var errorMsg: any = new Error();
                errorMsg.message = fileAccessDenied ? ((permission.message !== "") ? permission.message :
                    item.name + " is not accessible. You need permission to perform the copy action.") :
                    ((pathPermission.message !== "") ? pathPermission.message :
                        ctx.request.body.targetData.name + " is not accessible. You need permission to perform the writeContents action.");
                errorMsg.code = "401";
                result = { error: errorMsg };
                result = JSON.stringify(result);
                return result;
            }
        });
        if (!permissionDenied) {
            await ctx.request.body.data.forEach((item) => {
                var fromPath: any = contentRootPath + item.filterPath + item.name;
                var toPath: any = contentRootPath + ctx.request.body.targetPath + item.name;
                checkForFileUpdate(fromPath, toPath, item, contentRootPath, ctx);
                if (!isRenameChecking) {
                    toPath = contentRootPath + ctx.request.body.targetPath + copyName;
                    if (item.isFile) {
                        fs.copyFileSync(path.join(fromPath), path.join(toPath));
                    }
                    else {
                        copyFolder(fromPath, toPath)
                    }
                    var list = item;
                    list.filterPath = ctx.request.body.targetPath;
                    list.name = copyName;
                    fileList.push(list);
                } else {
                    replaceFileList.push(item.name);
                }
            });
            if (replaceFileList.length == 0) {
                copyName = "";
                result = { files: fileList };
                result = JSON.stringify(result);
                return;
            } else {
                isRenameChecking = false;
                var errorMsg: any = new Error();
                errorMsg.message = "File Already Exists.";
                errorMsg.code = "400";
                errorMsg.fileExists = replaceFileList;
                result = { error: errorMsg, files: [] };
                result = JSON.stringify(result);
                return;
            }
        }
        return this.response;
    }

    /**
 * function move files and folder
 */
    public async MoveFiles(ctx, contentRootPath) {
        var result: any = {}
        var fileList: any[] = [];
        var replaceFileList: any[] = [];
        var permission; var pathPermission; var permissionDenied = false;
        pathPermission = getPathPermission(ctx.request.path, false, ctx.request.body.targetData.name, contentRootPath + ctx.request.body.targetPath, contentRootPath, ctx.request.body.targetData.filterPath);
        await ctx.request.body.data.forEach((item) => {
            var fromPath = contentRootPath + item.filterPath;
            permission = getPermission(fromPath, item.name, item.isFile, contentRootPath, item.filterPath);
            var fileAccessDenied = (permission != null && (!permission.read || !permission.write));
            var pathAccessDenied = (pathPermission != null && (!pathPermission.read || !pathPermission.writeContents));
            if (fileAccessDenied || pathAccessDenied) {
                permissionDenied = true;
                var errorMsg: any = new Error();
                errorMsg.message = fileAccessDenied ? ((permission.message !== "") ? permission.message :
                    item.name + " is not accessible. You need permission to perform the write action.") :
                    ((pathPermission.message !== "") ? pathPermission.message :
                        ctx.request.body.targetData.name + " is not accessible. You need permission to perform the writeContents action.");
                errorMsg.code = "401";
                result = { error: errorMsg };
                result = JSON.stringify(result);
                return result;

            }
        });
        if (!permissionDenied) {
            await ctx.request.body.data.forEach((item) => {
                var fromPath: any = contentRootPath + item.filterPath + item.name;
                var toPath = contentRootPath + ctx.request.body.targetPath + item.name;
                checkForFileUpdate(fromPath, toPath, item, contentRootPath, ctx);
                if (!isRenameChecking) {
                    toPath = contentRootPath + ctx.request.body.targetPath + copyName;
                    if (item.isFile) {
                        var source = fs.createReadStream(path.join(fromPath));
                        var desti = fs.createWriteStream(path.join(toPath));
                        source.pipe(desti);
                        source.on('end', () => {
                            fs.unlinkSync(path.join(fromPath));
                        });
                    }
                    else {
                        MoveFolder(fromPath, toPath);
                        fs.rmdirSync(fromPath);
                    }
                    var list = item;
                    list.name = copyName;
                    list.filterPath = ctx.request.body.targetPath;
                    fileList.push(list);
                } else {
                    replaceFileList.push(item.name);
                }
            });
            if (replaceFileList.length == 0) {
                copyName = "";
                result = { files: fileList };
                result = JSON.stringify(result);
                return;

            }
            else {
                isRenameChecking = false;
                var errorMsg: any = new Error();
                errorMsg.message = "File Already Exists.";
                errorMsg.code = "400";
                errorMsg.fileExists = replaceFileList;
                result = { error: errorMsg, files: [] };
                result = JSON.stringify(result);
                return;

            }
        }
        return this.response;
    }
    /**
     * function to create the folder
     */
    public async createFolder(ctx, filepath, contentRootPath) {

        var newDirectoryPath = path.join(contentRootPath + ctx.request.body.path, ctx.request.body.name);
        var pathPermission = getPathPermission(ctx.request.path, false, ctx.request.body.data[0].name, filepath, contentRootPath, ctx.request.body.data[0].filterPath);
        if (pathPermission != null && (!pathPermission.read || !pathPermission.writeContents)) {
            var errorMsg: any = new Error();
            errorMsg.message = Permission + ctx.request.body.data[0].name + " is not accessible. You need permission to perform the writeContents action.";
            errorMsg.code = "401";
            this.response = { error: errorMsg };
            this.response = JSON.stringify(this.response);
            return;
        }
        else {
            if (fs.existsSync(newDirectoryPath)) {
                var errorMsg: any = new Error();
                errorMsg.message = "A file or folder with the name " + ctx.request.body.name + " already exists.";
                errorMsg.code = "400";
                this.response = { error: errorMsg };
                this.response = JSON.stringify(this.response);
                return;

            } else {
                fs.mkdirSync(newDirectoryPath);
                await fileManagerDirectoryContent(ctx, newDirectoryPath).then(data => {
                    this.response = { files: data };
                    this.response = JSON.stringify(this.response);
                    return;
                });
            }
        }
        return this.response;
    }
    /**
 * function to delete the folder
 */
    public async deleteFolder(ctx, contentRootPath) {
        var result: any = {}
        var deleteFolderRecursive = (path) => {
            if (fs.existsSync(path)) {
                fs.readdirSync(path).forEach((file, index) => {
                    var curPath = path + "/" + file;
                    if (fs.lstatSync(curPath).isDirectory()) { // recurse
                        deleteFolderRecursive(curPath);
                    } else { // delete file
                        fs.unlinkSync(curPath);
                    }
                });
                fs.rmdirSync(path);
            }
        };
        var permission; var permissionDenied = false;
        await ctx.request.body.data.forEach((item) => {
            var fromPath = contentRootPath + item.filterPath;
            permission = getPermission(fromPath, item.name, item.isFile, contentRootPath, item.filterPath);
            if (permission != null && (!permission.read || !permission.write)) {
                permissionDenied = true;
                var errorMsg: any = new Error();
                errorMsg.message = (permission.message !== "") ? permission.message : item.name + " is not accessible. You need permission to perform the write action.";
                errorMsg.code = "401";
                result = { error: errorMsg };
                result = JSON.stringify(result);
                return;
            }
        });
        if (!permissionDenied) {
            var promiseList: any[] = [];
            for (var i = 0; i < ctx.request.body.data.length; i++) {
                var newDirectoryPath = path.join(contentRootPath + ctx.request.body.data[i].filterPath, ctx.request.body.data[i].name);
                if (fs.lstatSync(newDirectoryPath).isFile()) {
                    promiseList.push(fileManagerDirectoryContent(ctx, newDirectoryPath, ctx.request.body.data[i].filterPath));
                } else {
                    promiseList.push(fileManagerDirectoryContent(ctx, newDirectoryPath + "/", ctx.request.body.data[i].filterPath));
                }
            }
            await Promise.all(promiseList).then(data => {
                data.forEach((files: any) => {
                    if (fs.lstatSync(path.join(contentRootPath + files.filterPath, files.name)).isFile()) {
                        fs.unlinkSync(path.join(contentRootPath + files.filterPath, files.name));
                    } else {
                        deleteFolderRecursive(path.join(contentRootPath + files.filterPath, files.name));
                    }
                });
                result = { files: data };
                result = JSON.stringify(result);
                this.response = result;
                return;
            });
        }
        return this.response;
    }
    /**
 * function to rename the folder
 */
    public async renameFolder(ctx, contentRootPath) {
        var filename = ctx.request.path.replace(/^.*[\\\/]/, '')
        var oldName = ctx.request.body.data[0].name.split("/")[ctx.request.body.data[0].name.split("/").length - 1];
        var newName = ctx.request.body.newName.split("/")[ctx.request.body.newName.split("/").length - 1];
        var permission = getPermission((contentRootPath + ctx.request.body.data[0].filterPath), oldName, ctx.request.body.data[0].isFile, contentRootPath, ctx.request.body.data[0].filterPath);
        if (permission != null && (!permission.read || !permission.write)) {
            var errorMsg: any = new Error();
            errorMsg.message = contentRootPath + ctx.request.body.data[0].filterPath + oldName + " is not accessible.  is not accessible. You need permission to perform the write action.";
            errorMsg.code = "401";
            this.response = { error: errorMsg };
            this.response = JSON.stringify(this.response);
            return;

        } else {
            var oldDirectoryPath = path.join(contentRootPath + ctx.request.body.data[0].filterPath, oldName);
            var newDirectoryPath = path.join(contentRootPath + ctx.request.body.data[0].filterPath, newName);
            if (checkForDuplicates(contentRootPath + ctx.request.body.data[0].filterPath, newName, ctx.request.body.data[0].isFile)) {
                var errorMsg: any = new Error();
                errorMsg.message = "A file or folder with the name " + ctx.request.body.name + " already exists.";
                errorMsg.code = "400";
                this.response = { error: errorMsg };
                this.response = JSON.stringify(this.response);
                return;

            } else {
                fs.renameSync(oldDirectoryPath, newDirectoryPath);
                (async () => {
                    await fileManagerDirectoryContent(ctx, newDirectoryPath + "/").then(data => {
                        this.response = { files: data };
                        this.response = JSON.stringify(this.response);
                        return;
                    });
                })();
            }
        }
        return this.response;
    }

}
/**
 * function to get the folder size
 */
function getFolderSize(ctx, directory, sizeValue) {
    size = sizeValue;
    var filenames = fs.readdirSync(directory);
    for (var i = 0; i < filenames.length; i++) {
        if (fs.lstatSync(directory + "/" + filenames[i]).isDirectory()) {
            getFolderSize(ctx, directory + "/" + filenames[i], size);
        } else {
            const stats = fs.statSync(directory + "/" + filenames[i]);
            size = size + stats.size;
        }
    }
}

function checkForMultipleLocations(ctx, contentRootPath) {
    var previousLocation = "";
    var isMultipleLocation = false;
    ctx.request.body.data.forEach((item) => {
        if (previousLocation == "") {
            previousLocation = item.filterPath;
            location = item.filterPath;
        } else if (previousLocation == item.filterPath && !isMultipleLocation) {
            isMultipleLocation = false;
            location = item.filterPath;
        } else {
            isMultipleLocation = true;
            location = "Various Location";
        }
    });
    if (!isMultipleLocation) {
        location = contentRootPath.split("/")[contentRootPath.split("/").length - 1] + location.substr(0, location.length - 2);
    }
    return isMultipleLocation;
}

/**
 *
 * function to check for exising folder or file
 */
function checkForDuplicates(directory, name, isFile) {
    var filenames = fs.readdirSync(directory);
    if (filenames.indexOf(name) == -1) {
        return false;
    } else {
        for (var i = 0; i < filenames.length; i++) {
            if (filenames[i] === name) {
                if (!isFile && fs.lstatSync(directory + "/" + filenames[i]).isDirectory()) {
                    return true;
                } else if (isFile && !fs.lstatSync(directory + "/" + filenames[i]).isDirectory()) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}

function updateCopyName(path, name, count, isFile) {
    var subName = "", extension = "";
    if (isFile) {
        extension = name.substr(name.lastIndexOf('.'), name.length - 1);
        subName = name.substr(0, name.lastIndexOf('.'));
    }
    copyName = !isFile ? name + "(" + count + ")" : (subName + "(" + count + ")" + extension);
    if (checkForDuplicates(path, copyName, isFile)) {
        count = count + 1;
        updateCopyName(path, name, count, isFile);
    }
}

function checkForFileUpdate(fromPath, toPath, item, contentRootPath, ctx) {
    var count = 1;
    var name = copyName = item.name;
    if (fromPath == toPath) {
        if (checkForDuplicates(contentRootPath + ctx.request.body.targetPath, name, item.isFile)) {
            updateCopyName(contentRootPath + ctx.request.body.targetPath, name, count, item.isFile);
        }
    } else {
        if (ctx.request.body.renameFiles.length > 0 && ctx.request.body.renameFiles.indexOf(item.name) >= 0) {
            updateCopyName(contentRootPath + ctx.request.body.targetPath, name, count, item.isFile);
        } else {
            if (checkForDuplicates(contentRootPath + ctx.request.body.targetPath, name, item.isFile)) {
                isRenameChecking = true;
            }
        }
    }
}

function copyFolder(source, dest) {
    if (!fs.existsSync(dest)) {
        fs.mkdirSync(dest);
    }
    var files = fs.readdirSync(source);
    files.forEach((file) => {
        var curSource = path.join(source, file);
        if (fs.lstatSync(curSource).isDirectory()) {
            copyFolder(curSource, path.join(dest, file)); source
        } else {
            fs.copyFileSync(path.join(source, file), path.join(dest, file));
        }
    });
}

function MoveFolder(source, dest) {
    if (!fs.existsSync(dest)) {
        fs.mkdirSync(dest);
    }
    var files = fs.readdirSync(source);
    files.forEach((file) => {
        var curSource = path.join(source, file);
        if (fs.lstatSync(curSource).isDirectory()) {
            MoveFolder(curSource, path.join(dest, file));
            fs.rmdirSync(curSource);
        } else {
            fs.copyFileSync(path.join(source, file), path.join(dest, file));
            fs.unlinkSync(path.join(source, file));
        }
    });
}

