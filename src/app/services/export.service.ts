import * as express from 'express';
import * as bodyParser from "body-parser";
import * as path from 'path';
import * as fs from "fs";

const contentRootPath = ".\\public\\";
const pattern = /(\.\.\/)/g;
var Permission = {
    Allow: "allow",
    Deny: "deny"
};
var accessDetails: any = null;
var size = 0;

export class AccessDetails {
    public role
    public rules

    constructor(role?, rules?) {
        this.role = role;
        this.rules = rules;
    }
}

export class AccessPermission {
    public read = Boolean;
    public write = Boolean;
    public writeContents = Boolean;
    public copy = Boolean;
    public download = Boolean;
    public upload = Boolean;
    public message = String;

    constructor(read, write, writeContents, copy, download, upload, message) {
        this.read = read;
        this.write = write;
        this.writeContents = writeContents;
        this.copy = copy;
        this.download = download;
        this.upload = upload;
        this.message = message
    }
}

export class AccessRules {
    public path
    public role
    public read
    public write
    public writeContents
    public copy
    public download
    public upload
    public isFile
    public message

    constructor(path, role, read, write, writeContents, copy, download, upload, isFile, message) {
        this.path = path;
        this.role = role;
        this.read = read;
        this.write = write;
        this.writeContents = writeContents;
        this.copy = copy;
        this.download = download;
        this.upload = upload;
        this.isFile = isFile;
        this.message = message
    }
}

export function parentsHavePermission(filepath, contentRootPath, isFile, name, filterPath): any {
    var parentPath = filepath.substr(contentRootPath.length, filepath.length - 1).replace(/\\/g, "/");
    parentPath = parentPath.substr(0, parentPath.indexOf(name)) + (isFile ? "" : "/");
    var parents = parentPath.split('/');
    var currPath = "/";
    var hasPermission = true;
    var pathPermission;
    for (var i = 0; i <= parents.length - 2; i++) {
        currPath = (parents[i] == "") ? currPath : (currPath + parents[i] + "/");
        pathPermission = getPathPermission(parentPath, false, parents[i], contentRootPath + (currPath == "/" ? "" : "/"), contentRootPath, filterPath);
        if (pathPermission == null) {
            break;
        }
        else if (pathPermission != null && !pathPermission.read) {
            hasPermission = false;
            break;
        }
    }
    return hasPermission;
}

export function getPermission(filepath, name, isFile, contentRootPath, filterPath) {
    var filePermission: any = new AccessPermission(true, true, true, true, true, true, "");
    if (accessDetails == null) {
        return null;
    } else {
        accessDetails.rules.forEach(function(accessRule) {
            if (isFile && accessRule.isFile) {
                var nameExtension = name.substr(name.lastIndexOf("."), name.length - 1).toLowerCase();
                var fileName = name.substr(0, name.lastIndexOf("."));
                var currentPath = contentRootPath + filterPath;
                if (accessRule.isFile && isFile && accessRule.path != "" && accessRule.path != null && (accessRule.role == null || accessRule.role == accessDetails.role)) {
                    if (accessRule.path.indexOf("*.*") > -1) {
                        var parentPath = accessRule.path.substr(0, accessRule.path.indexOf("*.*"));
                        if (currentPath.indexOf(contentRootPath + parentPath) == 0 || parentPath == "") {
                            filePermission = updateRules(filePermission, accessRule);
                        }
                    }
                    else if (accessRule.path.indexOf("*.") > -1) {
                        var pathExtension = accessRule.path.substr(accessRule.path.lastIndexOf("."), accessRule.path.length - 1).toLowerCase();
                        var parentPath = accessRule.path.substr(0, accessRule.path.indexOf("*."));
                        if (((contentRootPath + parentPath) == currentPath || parentPath == "") && nameExtension == pathExtension) {
                            filePermission = updateRules(filePermission, accessRule);
                        }
                    }
                    else if (accessRule.path.indexOf(".*") > -1) {
                        var pathName = accessRule.path.substr(0, accessRule.path.lastIndexOf(".")).substr(accessRule.path.lastIndexOf("/") + 1, accessRule.path.length - 1);
                        var parentPath = accessRule.path.substr(0, accessRule.path.indexOf(pathName + ".*"));
                        if (((contentRootPath + parentPath) == currentPath || parentPath == "") && fileName == pathName) {
                            filePermission = updateRules(filePermission, accessRule);
                        }
                    }
                    else if (contentRootPath + accessRule.path == filepath) {
                        filePermission = updateRules(filePermission, accessRule);
                    }
                }
            } else {
                if (!accessRule.isFile && !isFile && accessRule.path != null && (accessRule.role == null || accessRule.role == accessDetails.role)) {
                    var parentFolderpath = contentRootPath + filterPath;
                    if (accessRule.path.indexOf("*") > -1) {
                        var parentPath = accessRule.path.substr(0, accessRule.path.indexOf("*"));
                        if (((parentFolderpath + (parentFolderpath[parentFolderpath.length - 1] == "/" ? "" : "/") + name).lastIndexOf(contentRootPath + parentPath) == 0) || parentPath == "") {
                            filePermission = updateRules(filePermission, accessRule);
                        }
                    } else if (path.join(contentRootPath, accessRule.path) == path.join(parentFolderpath, name) || path.join(contentRootPath, accessRule.path) == path.join(parentFolderpath, name + "/")) {
                        filePermission = updateRules(filePermission, accessRule);
                    }
                    else if (path.join(parentFolderpath, name).lastIndexOf(path.join(contentRootPath, accessRule.path)) == 0) {
                        filePermission.write = hasPermission(accessRule.writeContents);
                        filePermission.writeContents = hasPermission(accessRule.writeContents);
                        filePermission.message = getMessage(accessRule);
                    }
                }
            }
        });
        return filePermission;
    }
}

export function getSize(size) {
    var hz;
    if (size < 1024) hz = size + ' B';
    else if (size < 1024 * 1024) hz = (size / 1024).toFixed(2) + ' KB';
    else if (size < 1024 * 1024 * 1024) hz = (size / 1024 / 1024).toFixed(2) + ' MB';
    else hz = (size / 1024 / 1024 / 1024).toFixed(2) + ' GB';
    return hz;
}
/**
* returns the current working directories
*/
export function fileManagerDirectoryContent(ctx, filepath, searchFilterPath?) {
    return new Promise((resolve, reject) => {
        var cwd: any = {};
        replaceRequestParams(ctx);
        fs.stat(filepath, function(err, stats) {
            cwd.name = path.basename(filepath);

            cwd.size = getSize(stats.size);
            cwd.isFile = stats.isFile();
            cwd.dateModified = stats.ctime;
            cwd.dateCreated = stats.mtime;
            cwd.type = path.extname(filepath);
            if (searchFilterPath) {
                cwd.filterPath = searchFilterPath;
            } else {
                cwd.filterPath = ctx.request.body.data.length > 0 ? getRelativePath(contentRootPath, contentRootPath + ctx.request.body.path.substring(0, ctx.request.body.path.indexOf(ctx.request.body.data[0].name))) : "";
            }
            cwd.permission = getPathPermission(ctx.request.path, cwd.isFile, (ctx.request.body.path == "/") ? "" : cwd.name, filepath, contentRootPath, cwd.filterPath);
            if (fs.lstatSync(filepath).isFile()) {
                cwd.hasChild = false;
                resolve(cwd);
            }
        });
        if (fs.lstatSync(filepath).isDirectory()) {
            fs.readdir(filepath, function(err, stats) {
                stats.forEach(stat => {
                    if (fs.lstatSync(filepath + stat).isDirectory()) {
                        cwd.hasChild = true
                    } else {
                        cwd.hasChild = false;
                    }
                    if (cwd.hasChild) return;
                });
                resolve(cwd);
            });
        }
    });
}

export function readDirectories(file, ctx) {
    var cwd = {};
    var directoryList: any = [];
    function stats(file) {
        return new Promise((resolve, reject) => {
            fs.stat(file, (err, cwd: any) => {
                if (err) {
                    return reject(err);
                }
                cwd.name = path.basename(contentRootPath + ctx.request.body.path + file);
                cwd.size = (cwd.size);
                cwd.isFile = cwd.isFile();
                cwd.dateModified = cwd.ctime;
                cwd.dateCreated = cwd.mtime;
                cwd.filterPath = getRelativePath(contentRootPath, contentRootPath + ctx.request.body.path);
                cwd.type = path.extname(contentRootPath + ctx.request.body.path + file);
                cwd.permission = getPermission(contentRootPath + ctx.request.body.path + cwd.name, cwd.name, cwd.isFile, contentRootPath, cwd.filterPath);
                if (fs.lstatSync(file).isDirectory()) {
                    fs.readdirSync(file).forEach(function(items) {
                        if (fs.statSync(path.join(file, items)).isDirectory()) {
                            directoryList.push(items[i]);
                        }
                        if (directoryList.length > 0) {
                            cwd.hasChild = true;
                        } else {
                            cwd.hasChild = false;
                            directoryList = [];
                        }
                    });
                } else {
                    cwd.hasChild = false;
                    var dir = [];
                }
                directoryList = [];
                resolve(cwd);
            });
        });
    }
    var promiseList: any[] = [];
    for (var i = 0; i < file.length; i++) {
        promiseList.push(stats(path.join(contentRootPath + ctx.request.body.path.replace(pattern, ""), file[i])));
    }
    return Promise.all(promiseList);
}

export function fromDir(fileList, startPath, filter, contentRootPath, casesensitive, searchString) {
    if (!fs.existsSync(startPath)) {
        return;
    }
    var files = fs.readdirSync(startPath);
    for (var i = 0; i < files.length; i++) {
        var filename = path.join(startPath, files[i]);
        var stat = fs.lstatSync(filename);
        if (stat.isDirectory()) {
            if (checkForSearchResult(casesensitive, filter, false, files[i], searchString)) {
                addSearchList(filename, contentRootPath, fileList, files, i);
            }
            fromDir(fileList, filename, filter, contentRootPath, casesensitive, searchString); //recurse
        }
        else if (checkForSearchResult(casesensitive, filter, true, files[i], searchString)) {
            addSearchList(filename, contentRootPath, fileList, files, i);
        }
    }
    function checkForSearchResult(casesensitive, filter, isFile, fileName, searchString) {
        var isAddable = false;
        if (searchString.substr(0, 1) == "*" && searchString.substr(searchString.length - 1, 1) == "*") {
            if (casesensitive ? fileName.indexOf(filter) >= 0 : (fileName.indexOf(filter.toLowerCase()) >= 0 || fileName.indexOf(filter.toUpperCase()) >= 0)) {
                isAddable = true
            }
        } else if (searchString.substr(searchString.length - 1, 1) == "*") {
            if (casesensitive ? fileName.startsWith(filter) : (fileName.startsWith(filter.toLowerCase()) || fileName.startsWith(filter.toUpperCase()))) {
                isAddable = true
            }
        } else {
            if (casesensitive ? fileName.endsWith(filter) : (fileName.endsWith(filter.toLowerCase()) || fileName.endsWith(filter.toUpperCase()))) {
                isAddable = true
            }
        }
        return isAddable;
    }
    function addSearchList(filename, contentRootPath, fileList, files, index) {
        var cwd: any = {};
        var stats = fs.statSync(filename);
        cwd.name = path.basename(filename);
        cwd.size = stats.size;
        cwd.isFile = stats.isFile();
        cwd.dateModified = stats.mtime;
        cwd.dateCreated = stats.ctime;
        cwd.type = path.extname(filename);
        cwd.filterPath = filename.substr((contentRootPath.length), filename.length).replace(files[index], "");
        cwd.permission = getPermission(filename.replace(/\\/g, "/"), cwd.name, cwd.isFile, contentRootPath, cwd.filterPath);
        var permission = parentsHavePermission(filename, contentRootPath, cwd.isFile, cwd.name, cwd.filterPath);
        if (permission) {
            if (fs.lstatSync(filename).isFile()) {
                cwd.hasChild = false;
            }
            if (fs.lstatSync(filename).isDirectory()) {
                var statsRead = fs.readdirSync(filename);
                cwd.hasChild = statsRead.length > 0;
            }
            fileList.push(cwd);
        }
    }
}
/**
 * function to get the folder size
 */
export function getFolderSize(ctx, directory, sizeValue) {
    size = sizeValue;
    var filenames = fs.readdirSync(directory);
    for (var i = 0; i < filenames.length; i++) {
        if (fs.lstatSync(directory + "/" + filenames[i]).isDirectory()) {
            getFolderSize(ctx, directory + "/" + filenames[i], size);
        } else {
            const stats = fs.statSync(directory + "/" + filenames[i]);
            size = size + stats.size;
        }
    }
    return size;
}
/**
 * function to get the file details like path, name and size
 */
export function fileDetails(ctx, filepath) {
    return new Promise((resolve, reject) => {
        var cwd: any = {};
        fs.stat(filepath, function(err, stats) {
            cwd.name = path.basename(filepath);
            cwd.size = getSize(stats.size);
            cwd.isFile = stats.isFile();
            cwd.modified = stats.ctime;
            cwd.created = stats.mtime;
            cwd.type = path.extname(filepath);
            cwd.location = ctx.request.body.data[0].filterPath
            resolve(cwd);
        });
    });
}

export function updateRules(filePermission, accessRule) {
    filePermission.download = hasPermission(accessRule.read) && hasPermission(accessRule.download);
    filePermission.write = hasPermission(accessRule.read) && hasPermission(accessRule.write);
    filePermission.writeContents = hasPermission(accessRule.read) && hasPermission(accessRule.writeContents);
    filePermission.copy = hasPermission(accessRule.read) && hasPermission(accessRule.copy);
    filePermission.read = hasPermission(accessRule.read);
    filePermission.upload = hasPermission(accessRule.read) && hasPermission(accessRule.upload);
    filePermission.message = getMessage(accessRule);
    return filePermission;
}

export function hasPermission(rule) {
    return ((rule == undefined) || (rule == null) || (rule == Permission.Allow)) ? true : false;
}

export function getMessage(rule) {
    return ((rule.message == undefined) || (rule.message == null)) ? "" : rule.message;
}

export function replaceRequestParams(ctx) {
    return ctx.request.body.path = (ctx.request.body.path && ctx.request.body.path.replace(pattern, ""));
}

export function getPathPermission(path, isFile, name, filepath, contentRootPath, filterPath) {
    return getPermission(filepath, name, isFile, contentRootPath, filterPath);
}

export function getRelativePath(rootDirectory, fullPath) {
    if (rootDirectory.substring(rootDirectory.length - 1) == "/") {
        if (fullPath.indexOf(rootDirectory) >= 0) {
            return fullPath.substring(rootDirectory.length - 1);
        }
    }
    else if (fullPath.indexOf(rootDirectory + "/") >= 0) {
        return "/" + fullPath.substring(rootDirectory.length + 1);
    }
    else {
        return "";
    }
}