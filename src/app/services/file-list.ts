import * as fs from "fs";
import * as path from "path";

export class FileList {
    public pending: string[];
    public pendingDirectory: string[];
    public directory: string[];
    public file: (string | { filename: string; stats: fs.Stats })[];
    public onComplete?: Function | null;
    public onFilter?: Function | null;
    public advanced: boolean;

    constructor() {
        this.pending = [];
        this.pendingDirectory = [];
        this.directory = [];
        this.file = [];
        this.onComplete = null;
        this.onFilter = null;
        this.advanced = false;
    }

    public reset(): this {
        this.file = [];
        this.directory = [];
        this.pendingDirectory = [];
        return this;
    }

    public walk(directory: string[] | string): string | undefined {
        if (directory instanceof Array) {
            for (let i = 0, len = directory.length; i < len; i++) {
                this.pendingDirectory.push(directory[i]);
            }
            this.next();
            return;
        }
        fs.readdir(directory, (err: NodeJS.ErrnoException | null, arr: string[]) => {
            if (err) {
                return this.next();
            }
            for (let i = 0, len = arr.length; i < len; i++) {
                this.pending.push(path.join(directory, arr[i]));
            }
            this.next();
        });
    }

    public stat(exactPath: string): void {
        fs.stat(exactPath, (err: NodeJS.ErrnoException | null, stats: fs.Stats) => {
            if (err) {
                return this.next();
            }

            if (stats.isDirectory()) {
                exactPath = this.clean(exactPath);
                if (!this.onFilter || this.onFilter(exactPath, true)) {
                    this.directory.push(exactPath);
                    this.pendingDirectory.push(exactPath);
                }
            } else if (!this.onFilter || this.onFilter(exactPath, false)) {
                this.file.push(this.advanced ? { filename: exactPath, stats } : exactPath);
            }

            this.next();
        });
    }

    public clean(exactPath: string): string {
        return exactPath.endsWith("/") ? exactPath : exactPath + "/";
    }

    public next(): void {
        if (this.pending.length) {
            const item = this.pending.shift();
            if (item) {
                this.stat(item);
            }
            return;
        }

        if (this.pendingDirectory.length) {
            const directory = this.pendingDirectory.shift();
            if (directory) {
                this.walk(directory);
            }
            return;
        }

        if (this.onComplete) {
            this.onComplete(this.file, this.directory);
        }
    }
}
