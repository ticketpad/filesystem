import * as multer from 'multer';
import * as multerS3 from 'multer-s3';

const imageFilter = (req, file, next) => {
  if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
    return next(new Error("Please upload correct file type - png, jpg, jpeg"), false)
  }
  next(null, true);
};

export const Upload: any = multer({
  fileFilter: imageFilter,
  storage: multerS3({
    s3: {},
    bucket: 'some-bucket',
    metadata: function(req, file, cb) {
      console.log(file);
      cb(null, { fieldName: file.fieldname });
    },
    key: function(req, file, cb) {
      cb(null, Date.now().toString())
    }
  })
}).any("file");