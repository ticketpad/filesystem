import {
    Get, dependency, Context,
    Post, HttpResponseOK, HttpResponseBadRequest, HttpResponseNoContent,
    HttpResponseInternalServerError
} from "@foal/core";
import * as cors from 'cors';
import * as bodyParser from "body-parser";
import * as path from 'path';
import * as fs from "fs";
import { ValidateMultipartFormDataBody, Disk } from "@foal/storage"
import {
    System, getPermission, parentsHavePermission, fileManagerDirectoryContent,
    readDirectories, fromDir, replaceRequestParams, getPathPermission
} from "../services";
import { FileUtils } from "../services";
import * as zl from 'zip-lib';
// const element
const contentRootPath = ".\\public\\";
const pattern = /(\.\.\/)/g;
var accessDetails: any = null;
var Permission = {
    Allow: "allow",
    Deny: "deny"
};
var fileName: string[] = [];
const tmpUploadFolder = "./tmp";
// @JWTRequired()
export class FileSystemController {
    @dependency private system: System;
    @dependency private disk: Disk;
    @dependency public fileUtils: FileUtils;
    public resp: any;
    public filterPath: string;

    @Get("/fileSystem/GetImage")
    public async getImage(ctx): Promise<HttpResponseOK | HttpResponseBadRequest | null> {
        let image = ctx.request.query.path.split("/").length > 1 ? ctx.request.query.path : "/" + ctx.request.query.path;
        let pathPermission = getPermission(contentRootPath + image.substr(0, image.lastIndexOf("/")), image.substr(image.lastIndexOf("/") + 1, image.length - 1), true, contentRootPath, image.substr(0, image.lastIndexOf("/")));

        if (pathPermission != null && !pathPermission.read) {
            return null;
        }
        else {
            this.resp = this.fileUtils.readFileSync(contentRootPath + image)
            //<<<<------- 3 different buffer
            return new HttpResponseOK(this.resp);
        }
    }

    @Post("/fileSystem")
    public async Image(ctx): Promise<HttpResponseOK | HttpResponseBadRequest | HttpResponseInternalServerError> {
        let response: any = {};
        accessDetails = await this.system.getRules();
        try {
            switch (ctx.request.body.action) {
                case 'read':
                    const filesList = await this.system.GetFiles(ctx);
                    const cwdFiles: any = await fileManagerDirectoryContent(ctx, contentRootPath + ctx.request.body.path);

                    cwdFiles.name = path.basename(contentRootPath + ctx.request.body.path);
                    if (cwdFiles.permission != null && !cwdFiles.permission.read) {
                        var errorMsg = new Error();
                        errorMsg.message = (cwdFiles.permission.message !== "") ? cwdFiles.permission.message :
                            "'" + cwdFiles.name + "' is not accessible. You need permission to perform the read action.";
                        return new HttpResponseBadRequest(JSON.stringify(response))
                    }
                    const results = await readDirectories(filesList, ctx).then(data => {

                        response = { cwd: cwdFiles, files: data };
                        response = JSON.stringify(response);
                        return response;
                    });
                    break;

                case "details":
                    const detailResult = await this.system.getFileDetails(ctx, contentRootPath + ctx.request.body.path, ctx.request.body.data[0].filterPath);
                    response = detailResult;
                    break;

                case "copy":
                    const copyResult = await this.system.CopyFiles(ctx, contentRootPath);
                    response = copyResult;
                    break;

                case "move":
                    const moveResult = await this.system.MoveFiles(ctx, contentRootPath);
                    response = moveResult;
                    break;

                case "create":
                    const createResult = await this.system.createFolder(ctx, contentRootPath + ctx.request.body.path, contentRootPath);
                    response = createResult;
                    break;

                case "delete":
                    const deleteResult = await this.system.deleteFolder(ctx, contentRootPath);
                    response = deleteResult;
                    break;

                case "rename":
                    const renameResult = await this.system.renameFolder(ctx, contentRootPath + ctx.request.body.path);
                    response = renameResult;
                    break;

                case "search":
                    var fileList = [];

                    fromDir(fileList, contentRootPath + ctx.request.body.path, ctx.request.body.searchString.replace(/\*/g, ""), contentRootPath, ctx.request.body.caseSensitive, ctx.request.body.searchString);
                    const tes: any = await fileManagerDirectoryContent(ctx, contentRootPath + ctx.request.body.path);
                    if (tes.permission != null && !tes.permission.read) {
                        var errorMsg = new Error();
                        errorMsg.message =
                            "'" + contentRootPath + (ctx.request.body.path.substring(0, ctx.request.body.path.length - 1)) + "' is not accessible. You need permission to perform the read action.";
                        response = { error: errorMsg };
                        response = JSON.stringify(response);
                    } else {
                        response = { cwd: tes, files: fileList };
                        response = JSON.stringify(response);
                    }
                    break;

            }
            return new HttpResponseOK(response);
        } catch (e) {
            return new HttpResponseInternalServerError(e);
        }
    }
    /**
     * Handles the upload request
     */
    @Post("/fileSystem/UploadFile")
    @ValidateMultipartFormDataBody({
        files: {
            file: { required: true, saveTo: "." },
        }
    })
    async upload(ctx: Context) {
        console.log(ctx.request.body)
        let response: any = {};
        const { fields, files } = ctx.request.body;
        const name: string = ctx.request.body.files.file.filename;
        const filepath = ctx.request.body.files.file.path;
        const filterPath = ctx.request.body.fields.filterPath;
        fileName.push(name.replace(/[`~!@#$%^&*()|+\=?;:'",<>\{\}\[\]\\\/\s/]/gi, "_"));
        var pathPermission = getPathPermission(ctx.request.path, true, name, contentRootPath + filepath, contentRootPath, filepath);
        if (pathPermission != null && (!pathPermission.read || !pathPermission.upload)) {
            return new HttpResponseBadRequest(ctx.request.body.data.name + " is not accessible. You need permission to perform the upload action.");
        } else {
            if (filterPath === "null") {
                console.log("entry")
                for (var i = 0; i < fileName.length; i++) {
                    fs.rename(path.join(".\\tmp\\", filepath), path.join(contentRootPath, fileName[i]), function(err) {
                        if (err) throw err;
                    });
                }
            } else {
                for (var i = 0; i < fileName.length; i++) {
                    fs.rename(path.join(".\\tmp\\", filepath), path.join(contentRootPath, filterPath, "/", fileName[i]), function(err) {
                        if (err) throw err;
                    });
                }
            }
            response = "";
            fileName = [];
            return new HttpResponseOK(response);
        }
    }

    @Post("/fileSystem/Download")
    public async DownloadFile(ctx, res) {
        var downloadObj = JSON.parse(ctx.request.body.downloadInput)
        let response: any = {};
        replaceRequestParams(ctx);
        var permission; var permissionDenied = false;
        downloadObj.data.forEach((item) => {
            var filepath = (contentRootPath + item.filterPath).replace(/\\/g, "/");
            permission = getPermission(filepath + item.name, item.name, item.isFile, contentRootPath, item.filterPath);
            if (permission != null && (!permission.read || !permission.download)) {
                permissionDenied = true;
                var errorMsg: any = new Error();
                errorMsg.message = contentRootPath + item.filterPath + item.name + " is not accessible. You need permission to perform the download action.";
                errorMsg.code = "401";
                response = { error: errorMsg };
                response = JSON.stringify(response);
            }
        });
        if (!permissionDenied) {
            if (downloadObj.names.length === 1 && downloadObj.data[0].isFile) {
                var file = downloadObj.names[0];
                return this.disk.createHttpResponse(file, {
                    forceDownload: true,
                    filename: file
                });
            } else {
                return new HttpResponseNoContent();
            }
        }
        return new HttpResponseNoContent();
    };
}

