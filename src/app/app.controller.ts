import { Context, controller, IAppController, Options, HttpResponseNoContent } from '@foal/core';
import { createConnection } from 'typeorm';
import { FileSystemController } from './controllers';
import { Cors } from "./hooks";
import * as cors from 'cors';
import * as express from 'express';
import * as bodyParser from "body-parser";


const app = express();
const allowedOrigins = ['*'];

const options: cors.CorsOptions = {
  origin: allowedOrigins
};
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(cors(options));
@Cors()

export class AppController implements IAppController {
  subControllers = [
    controller('/api', FileSystemController),
  ];

  async init() {
    await createConnection();
  }

  @Options("*")
  public options(ctx: Context): HttpResponseNoContent {
    const response = new HttpResponseNoContent();
    // const response = new HttpResponseOK();
    response.setHeader("Access-Control-Allow-Methods", "HEAD, GET, POST, PUT, PATCH, DELETE");

    // You may need to allow other headers depending on what you need.
    response.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
    return response;
  }
}
