import { Hook, HookDecorator } from "@foal/core";

export function Cors(allowedURLs?: string): HookDecorator {
    return Hook( ctx => response => {
        // Every response of this controller and its sub-controllers will be added this header.
        response.setHeader("Access-Control-Allow-Origin", allowedURLs || ctx.request.get("Origin") || "*");
    });
}
