export enum SessionName {
    UserId = "userId",
    IP = "ip",
    Language = "language",
    Currency = "currency",
    ReferralUrl = "referralUrl",
    TransactionId = "transactionId",
    PaymentErrorMsg = "paymentErrorMsg",
    FrontJWTToken = "frontJWTToken",
    CaptchaToken = "captchaToken",
    CsrfToken = "csrfToken",
}

export interface SessionType {
    sessionToken: string;
}